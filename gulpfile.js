const gulp = require('gulp');
const replace = require('gulp-replace');
const gulpEbDeploy = require('gulp-elasticbeanstalk-deploy');

const fs = require('fs');
const AWS = require('aws-sdk');

const AWS_REGION = 'us-east-1';

const secretsManager = new AWS.SecretsManager({
  region: AWS_REGION,
});

const beanstalkEnvNames = {
  dev: 'ibanonline-padron-api-dev',
  staging: 'ibanonline-padron-api-staging',
  prod: 'ibanonline-padron-api-prod',
};

const {
  AWS_ACCESS_KEY_ID,
  AWS_SECRET_ACCESS_KEY,
  IMAGE_NAME,
  IMAGE_VERSION,
} = process.env;

const replaceDockerrun = (imageName) => {
  const fn = () => gulp
    .src(['Dockerrun.aws.json'])
    .pipe(replace('<image-name>', imageName))
    .pipe(gulp.dest('./'));
  fn.displayName = 'Replace Dockerrun.aws.json';

  return fn;
};

const deployer = (zipName, beanstalkEnvName) => {
  
  const fn = () => gulp
    .src(
      ['Dockerrun.aws.json'],
      { base: './' },
    )
    .pipe(gulpEbDeploy({
      name: 'ibanonline-padron-api',
      timestamp: false,
      version: zipName,
      waitForDeploy: true,
      amazon: {
        accessKeyId: AWS_ACCESS_KEY_ID,
        secretAccessKey: AWS_SECRET_ACCESS_KEY,
        signatureVersion: 'v4',
        region: AWS_REGION,
        bucket: 'ibanonline-elasticbeanstalk-apps',
        applicationName: 'ibanonline-padron-api',
        environmentName: beanstalkEnvName,
      },
    }));
  fn.displayName = 'Deploy to beanstalk';

  return fn;
};

gulp.task('deploy:dev', cb => gulp.series(
  replaceDockerrun(IMAGE_NAME),
  deployer(IMAGE_VERSION, beanstalkEnvNames.dev),
)(cb));

gulp.task('deploy:staging', cb => gulp.series(
  replaceDockerrun(IMAGE_NAME),
  deployer(`${IMAGE_VERSION}-staging`, beanstalkEnvNames.staging),
)(cb));

gulp.task('deploy:prod', cb => gulp.series(
  replaceDockerrun(IMAGE_NAME),
  deployer(`${IMAGE_VERSION}-prod`, beanstalkEnvNames.prod),
)(cb));

gulp.task('retrieve-staging-credentials', async () => {
  const SecretId = 'IBAN_API_STAGING_PIPELINES_ENV_VARIABLES';
  const secrets = await secretsManager
    .getSecretValue({ SecretId })
    .promise()
    .then(r => JSON.parse(r.SecretString));

  const output = {
    type: 'mysql',
    host: secrets.TYPEORM_HOST,
    port: secrets.TYPEORM_PORT,
    username: secrets.TYPEORM_USERNAME,
    password: secrets.TYPEORM_PASSWORD,
    database: secrets.TYPEORM_DATABASE,
    migrations: [
      'migrations/*.ts',
    ],
    migrationsDir: 'migrations',
  };

  fs.writeFileSync('ormconfig.json', JSON.stringify(output));
});
