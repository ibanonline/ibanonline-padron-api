import { Controller, Get, Request, Param } from '@nestjs/common';
import { Crud } from '@nestjsx/crud';
import { ApiUseTags } from '@nestjs/swagger';

import { CeduladoService } from './cedulado.service';
import { Cedulados } from '../../entities/CEDULADOS.entity';

@Crud({
  model: {
    type: Cedulados,
  },
})
@ApiUseTags('cedulados')
@Controller('cedulados')
export class CeduladoController {
  constructor(public service: CeduladoService) {}

  // @Get()
  // getCeduladoByCedula(@Param() params) {
  //   return this.service.findCeduladoByCedula(params.cedula);
  // }
}
