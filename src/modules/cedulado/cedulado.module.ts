import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';

import { CeduladoService } from './cedulado.service';
import { CeduladoController } from './cedulado.controller';
import { Cedulados } from '../../entities/CEDULADOS.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Cedulados])],
  controllers: [CeduladoController],
  providers: [CeduladoService],
  exports: [TypeOrmModule],
})
export class CeduladoModule {}
