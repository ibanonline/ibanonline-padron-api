import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';

import { Cedulados } from '../../entities/CEDULADOS.entity';

@Injectable()
export class CeduladoService extends TypeOrmCrudService<Cedulados> {
  constructor(@InjectRepository(Cedulados) repo) {
    super(repo);
  }

  public findCeduladoByCedula(cedula: string): Promise<Cedulados> {
   
    //cedula = '223-01199638-8' 
    let checker : string=''
    let lastCedula: string=''
    let answer = 0;
    let mod = 0;
    let par=0;
    let impar=0;
    let sumImpar = 0;
    let sumPar = 0;

    lastCedula = cedula.replace(/-/gi,'');
    checker = lastCedula.substr(lastCedula.length - 1, 1);

    console.log(lastCedula)
    try {
     for(var i = 10; i>0; i--){
       mod = parseInt(lastCedula.substr(i,1))
       if ((i % 2)!=0){
         impar = mod *2
         if(impar >= 10){
           impar = impar-9
         }
         sumImpar = sumImpar + impar
       }
       else{
         sumPar = par + mod
       }
     }
      answer = 10 - ((sumPar + sumImpar) % 10);

      console.log(answer)
      console.log(checker)
      if (answer === parseInt(checker) || (answer ===10 && parseInt(checker) === 0)) {
        return this.repo.findOne({
          MUN_CED: lastCedula.substr(0, 3),
          SEQ_CED: lastCedula.substr(3, 7),
          VER_CED: lastCedula.substr(lastCedula.length - 1, 1),
        });
      }
    } catch (e) {}
  }
}
