//DATABASE
export const DATABASE_TYPE = 'mssql';
export const DATABASE_HOST = 'IBAN-Strix';
export const DATABASE_PORT = 1433;
export const DATABASE_USERNAME = 'sa';
export const DATABASE_PASSWORD = 'Superman12@';
export const DATABASE_NAME = 'BSNSS_PEOPLE_DB';
export const DATABASE_SHOULD_SYNCHRONIZE = false;

export const DATABASE_ENTITIES_PATHS = [
  __dirname + '/entities/*.entity{.ts,.js}',
];

export const DATABASE_MIGRATIONS_PATHS = [
  __dirname + '/migration/*.entity{.ts,.js}',
];
