import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import {
  DATABASE_HOST,
  DATABASE_PORT,
  DATABASE_USERNAME,
  DATABASE_PASSWORD,
  DATABASE_NAME,
  DATABASE_SHOULD_SYNCHRONIZE,
  DATABASE_MIGRATIONS_PATHS,
  DATABASE_ENTITIES_PATHS,
} from './constant';
import { CeduladoModule } from './modules/cedulado/cedulado.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mssql',
      host: DATABASE_HOST,
      port: DATABASE_PORT,
      username: DATABASE_USERNAME,
      password: DATABASE_PASSWORD,
      database: DATABASE_NAME,
      synchronize: DATABASE_SHOULD_SYNCHRONIZE,
      entities: DATABASE_ENTITIES_PATHS,
      migrations: DATABASE_MIGRATIONS_PATHS,
    }),
    CeduladoModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
