import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';

@Entity('PadronExterior', { schema: 'dbo' })
@Index('IX_PadronExterior', [
  'cod_municipio_local',
  'colegio_local',
  'Apellido1',
  'Apellido2',
  'Nombres',
])
export class PadronExterior {
  @Column('int', {
    nullable: false,
    name: 'CodPais',
  })
  CodPais: number;

  @Column('varchar', {
    nullable: true,
    length: 50,
    name: 'Pais',
  })
  Pais: string | null;

  @Column('int', {
    nullable: false,
    name: 'CodCentro',
  })
  CodCentro: number;

  @Column('varchar', {
    nullable: true,
    length: 50,
    name: 'OCLEE',
  })
  OCLEE: string | null;

  @Column('varchar', {
    nullable: false,
    primary: true,
    length: 3,
    name: 'mun_ced',
  })
  mun_ced: string;

  @Column('varchar', {
    nullable: false,
    primary: true,
    length: 7,
    name: 'seq_ced',
  })
  seq_ced: string;

  @Column('varchar', {
    nullable: false,
    primary: true,
    length: 1,
    name: 'ver_ced',
  })
  ver_ced: string;

  @Column('varchar', {
    nullable: false,
    length: 60,
    name: 'Nombres',
  })
  Nombres: string;

  @Column('varchar', {
    nullable: false,
    length: 40,
    name: 'Apellido1',
  })
  Apellido1: string;

  @Column('varchar', {
    nullable: true,
    length: 40,
    name: 'Apellido2',
  })
  Apellido2: string | null;

  @Column('varchar', {
    nullable: true,
    length: 50,
    name: 'zonapostal',
  })
  zonapostal: string | null;

  @Column('varchar', {
    nullable: true,
    length: 200,
    name: 'Direccion1',
  })
  Direccion1: string | null;

  @Column('varchar', {
    nullable: true,
    length: 200,
    name: 'Direccion2',
  })
  Direccion2: string | null;

  @Column('int', {
    nullable: true,
    name: 'CodNivel1',
  })
  CodNivel1: number | null;

  @Column('varchar', {
    nullable: true,
    length: 60,
    name: 'Nivel1',
  })
  Nivel1: string | null;

  @Column('int', {
    nullable: true,
    name: 'CodNivel2',
  })
  CodNivel2: number | null;

  @Column('varchar', {
    nullable: true,
    length: 60,
    name: 'Nivel2',
  })
  Nivel2: string | null;

  @Column('int', {
    nullable: true,
    name: 'CodNivel3',
  })
  CodNivel3: number | null;

  @Column('varchar', {
    nullable: true,
    length: 60,
    name: 'Nivel3',
  })
  Nivel3: string | null;

  @Column('varchar', {
    nullable: true,
    length: 100,
    name: 'Nivel1Zip',
  })
  Nivel1Zip: string | null;

  @Column('varchar', {
    nullable: true,
    length: 100,
    name: 'Nivel2Zip',
  })
  Nivel2Zip: string | null;

  @Column('varchar', {
    nullable: true,
    length: 100,
    name: 'Nivel3Zip',
  })
  Nivel3Zip: string | null;

  @Column('char', {
    nullable: true,
    name: 'Habil',
  })
  Habil: string | null;

  @Column('char', {
    nullable: true,
    name: 'EstatusCedulado',
  })
  EstatusCedulado: string | null;

  @Column('char', {
    nullable: true,
    name: 'EstatusEmpadronado',
  })
  EstatusEmpadronado: string | null;

  @Column('varchar', {
    nullable: true,
    length: 50,
    name: 'PadronOrigen',
  })
  PadronOrigen: string | null;

  @Column('varchar', {
    nullable: true,
    length: 50,
    name: 'EstatusInscripcion',
  })
  EstatusInscripcion: string | null;

  @Column('varchar', {
    nullable: true,
    length: 3,
    name: 'cod_municipio_local',
  })
  cod_municipio_local: string | null;

  @Column('varchar', {
    nullable: true,
    length: 5,
    name: 'colegio_local',
  })
  colegio_local: string | null;
}
