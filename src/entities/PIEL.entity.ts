import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';

@Entity('PIEL', { schema: 'dbo' })
export class Piel {
  @Column('numeric', {
    nullable: false,
    precision: 2,
    scale: 0,
    name: 'COD_PIEL',
  })
  COD_PIEL: number;

  @Column('varchar', {
    nullable: true,
    length: 20,
    name: 'DESCRIPCION',
  })
  DESCRIPCION: string | null;

  @Column('varchar', {
    nullable: true,
    length: 2,
    name: 'LETRA',
  })
  LETRA: string | null;
}
