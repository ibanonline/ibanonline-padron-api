import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';

@Entity('CIUDAD_SECCION', { schema: 'dbo' })
export class CiudadSeccion {
  @Column('varchar', {
    nullable: false,
    primary: true,
    length: 2,
    name: 'COD_CIUDAD',
  })
  COD_CIUDAD: string;

  @Column('varchar', {
    nullable: false,
    primary: true,
    length: 3,
    name: 'COD_MUNICIPIO',
  })
  COD_MUNICIPIO: string;

  @Column('varchar', {
    nullable: true,
    length: 3,
    name: 'COD_DISTRITO_MUNICIPAL',
  })
  COD_DISTRITO_MUNICIPAL: string | null;

  @Column('varchar', {
    nullable: true,
    length: 50,
    name: 'DESCRIPCION',
  })
  DESCRIPCION: string | null;

  @Column('numeric', {
    nullable: true,
    precision: 18,
    scale: 0,
    name: 'OFICIO',
  })
  OFICIO: number | null;

  @Column('varchar', {
    nullable: true,
    length: 1,
    name: 'ESTATUS',
  })
  ESTATUS: string | null;
}
