import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';

@Entity('RND_GENERATED_DATA2', { schema: 'dbo' })
export class RndGeneratedData2 {
  @Column('varchar', {
    nullable: false,
    length: 3,
    name: 'MUN_CED',
  })
  MUN_CED: string;

  @Column('varchar', {
    nullable: false,
    length: 7,
    name: 'SEQ_CED',
  })
  SEQ_CED: string;

  @Column('varchar', {
    nullable: false,
    length: 1,
    name: 'VER_CED',
  })
  VER_CED: string;

  @Column('varchar', {
    nullable: true,
    length: 8000,
    name: 'NOMBRES',
  })
  NOMBRES: string | null;

  @Column('varchar', {
    nullable: true,
    length: 8000,
    name: 'APELLIDO1',
  })
  APELLIDO1: string | null;
}
