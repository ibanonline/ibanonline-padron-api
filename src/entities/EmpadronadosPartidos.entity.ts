import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';

@Entity('EmpadronadosPartidos', { schema: 'dbo' })
export class EmpadronadosPartidos {
  @Column('varchar', {
    nullable: false,
    length: 3,
    name: 'mun_ced',
  })
  mun_ced: string;

  @Column('varchar', {
    nullable: false,
    length: 7,
    name: 'seq_ced',
  })
  seq_ced: string;

  @Column('varchar', {
    nullable: false,
    length: 1,
    name: 'ver_ced',
  })
  ver_ced: string;

  @Column('varchar', {
    nullable: false,
    length: 1,
    name: 'EstatusEmpadronado',
  })
  EstatusEmpadronado: string;

  @Column('varchar', {
    nullable: false,
    length: 25,
    name: 'SolicitudUsuario',
  })
  SolicitudUsuario: string;
}
