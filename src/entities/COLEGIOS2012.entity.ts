import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';

@Entity('COLEGIOS2012', { schema: 'dbo' })
export class Colegios2012 {
  @Column('varchar', {
    nullable: false,
    primary: true,
    length: 3,
    name: 'cod_municipio',
  })
  cod_municipio: string;

  @Column('varchar', {
    nullable: false,
    length: 5,
    name: 'cod_recinto',
  })
  cod_recinto: string;

  @Column('varchar', {
    nullable: false,
    primary: true,
    length: 6,
    name: 'colegio',
  })
  colegio: string;

  @Column('varchar', {
    nullable: true,
    length: 60,
    name: 'descripcion',
  })
  descripcion: string | null;

  @Column('int', {
    nullable: true,
    name: 'habiles',
  })
  habiles: number | null;

  @Column('int', {
    nullable: true,
    name: 'inhabilitados',
  })
  inhabilitados: number | null;
}
