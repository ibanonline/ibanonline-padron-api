import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';

@Entity('MUNICIPIO_EXTERIOR', { schema: 'dbo' })
export class MunicipioExterior {
  @Column('varchar', {
    nullable: false,
    primary: true,
    length: 3,
    name: 'COD_MUNICIPIO',
  })
  COD_MUNICIPIO: string;

  @Column('varchar', {
    nullable: true,
    length: 35,
    name: 'DESCRIPCION',
  })
  DESCRIPCION: string | null;

  @Column('varchar', {
    nullable: false,
    length: 2,
    name: 'COD_PROVINCIA',
  })
  COD_PROVINCIA: string;
}
