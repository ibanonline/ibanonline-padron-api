import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';

@Entity('CAUSAS_INHABILIDAD', { schema: 'dbo' })
export class CausasInhabilidad {
  @Column('numeric', {
    nullable: false,
    primary: true,
    precision: 2,
    scale: 0,
    name: 'COD_CAUSA',
  })
  COD_CAUSA: number;

  @Column('varchar', {
    nullable: false,
    primary: true,
    length: 2,
    name: 'TIPO_CAUSA',
  })
  TIPO_CAUSA: string;

  @Column('varchar', {
    nullable: true,
    length: 100,
    name: 'DESCRIPCION',
  })
  DESCRIPCION: string | null;

  @Column('varchar', {
    nullable: true,
    length: 1,
    name: 'AFECTA_COLEGIO',
  })
  AFECTA_COLEGIO: string | null;
}
