import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';

@Entity('PROVINCIA_EXTERIOR', { schema: 'dbo' })
export class ProvinciaExterior {
  @Column('varchar', {
    nullable: false,
    primary: true,
    length: 2,
    name: 'COD_PROVINCIA',
  })
  COD_PROVINCIA: string;

  @Column('varchar', {
    nullable: true,
    length: 30,
    name: 'DESCRIPCION',
  })
  DESCRIPCION: string | null;

  @Column('varchar', {
    nullable: true,
    length: 2,
    name: 'ZONA',
  })
  ZONA: string | null;
}
