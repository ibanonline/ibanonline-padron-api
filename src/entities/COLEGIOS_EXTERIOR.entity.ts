import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';

@Entity('COLEGIOS_EXTERIOR', { schema: 'dbo' })
export class ColegiosExterior {
  @Column('varchar', {
    nullable: true,
    length: 6,
    name: 'colegio',
  })
  colegio: string | null;

  @Column('varchar', {
    nullable: true,
    length: 3,
    name: 'cod_municipio',
  })
  cod_municipio: string | null;

  @Column('varchar', {
    nullable: true,
    length: 100,
    name: 'descripcion',
  })
  descripcion: string | null;

  @Column('varchar', {
    nullable: true,
    length: 5,
    name: 'cod_recinto',
  })
  cod_recinto: string | null;

  @Column('varchar', {
    nullable: false,
    length: 1,
    name: 'TIENECUPO',
  })
  TIENECUPO: string;

  @Column('int', {
    nullable: false,
    name: 'HABILES',
  })
  HABILES: number;

  @Column('int', {
    nullable: false,
    name: 'INHABILITADOS',
  })
  INHABILITADOS: number;
}
