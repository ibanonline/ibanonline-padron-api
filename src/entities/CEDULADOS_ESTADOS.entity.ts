import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';

@Entity('CEDULADOS_ESTADOS', { schema: 'dbo' })
export class CeduladosEstados {
  @Column('varchar', {
    nullable: false,
    primary: true,
    length: 1,
    name: 'ESTATUS',
  })
  ESTATUS: string;

  @Column('varchar', {
    nullable: true,
    length: 40,
    name: 'DESCRIPCION',
  })
  DESCRIPCION: string | null;
}
