import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';

@Entity('TIPOS_CAUSAS', { schema: 'dbo' })
export class TiposCausas {
  @Column('varchar', {
    nullable: false,
    primary: true,
    length: 2,
    name: 'TIPO_CAUSA',
  })
  TIPO_CAUSA: string;

  @Column('varchar', {
    nullable: true,
    length: 40,
    name: 'DESCRIPCION',
  })
  DESCRIPCION: string | null;
}
