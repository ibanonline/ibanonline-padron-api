import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';

@Entity('SECTOR_PARAJE', { schema: 'dbo' })
export class SectorParaje {
  @Column('varchar', {
    nullable: false,
    primary: true,
    length: 4,
    name: 'COD_SECTOR',
  })
  COD_SECTOR: string;

  @Column('varchar', {
    nullable: false,
    primary: true,
    length: 3,
    name: 'COD_MUNICIPIO',
  })
  COD_MUNICIPIO: string;

  @Column('varchar', {
    nullable: true,
    length: 70,
    name: 'DESCRIPCION',
  })
  DESCRIPCION: string | null;

  @Column('varchar', {
    nullable: false,
    length: 2,
    name: 'COD_CIUDAD',
  })
  COD_CIUDAD: string;

  @Column('numeric', {
    nullable: true,
    precision: 18,
    scale: 0,
    name: 'OFICIO',
  })
  OFICIO: number | null;

  @Column('varchar', {
    nullable: true,
    length: 1,
    name: 'ESTATUS',
  })
  ESTATUS: string | null;
}
