import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';

@Entity('OCUPACION', { schema: 'dbo' })
export class Ocupacion {
  @Column('numeric', {
    nullable: false,
    precision: 3,
    scale: 0,
    name: 'COD_OCUP',
  })
  COD_OCUP: number;

  @Column('varchar', {
    nullable: true,
    length: 25,
    name: 'DESCRIPCION',
  })
  DESCRIPCION: string | null;
}
