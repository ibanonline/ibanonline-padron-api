import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

@Entity('CEDULADOS', { schema: 'dbo' })
export class Cedulados {
  @ApiModelProperty()
  @Column('varchar', {
    nullable: false,
    primary: true,
    length: 3,
    name: 'MUN_CED',
  })
  MUN_CED: string;

  @ApiModelProperty()
  @Column('varchar', {
    nullable: false,
    primary: true,
    length: 7,
    name: 'SEQ_CED',
  })
  SEQ_CED: string;

  @ApiModelProperty()
  @Column('varchar', {
    nullable: false,
    primary: true,
    length: 1,
    name: 'VER_CED',
  })
  VER_CED: string;

  @ApiModelPropertyOptional()
  @Column('varchar', {
    nullable: true,
    length: 30,
    name: 'NOMBRES',
  })
  NOMBRES: string | null;

  @ApiModelProperty()
  @Column('varchar', {
    nullable: true,
    length: 30,
    name: 'APELLIDO1',
  })
  APELLIDO1: string | null;

  @ApiModelPropertyOptional()
  @Column('varchar', {
    nullable: true,
    length: 30,
    name: 'APELLIDO2',
  })
  APELLIDO2: string | null;

  @ApiModelProperty()
  @Column('datetime', {
    nullable: true,
    name: 'FECHA_NAC',
  })
  FECHA_NAC: Date | null;

  @ApiModelPropertyOptional()
  @Column('varchar', {
    nullable: true,
    length: 30,
    name: 'LUGAR_NAC',
  })
  LUGAR_NAC: string | null;

  @ApiModelProperty()
  @Column('varchar', {
    nullable: true,
    length: 6,
    name: 'CED_A_NUM',
  })
  CED_A_NUM: string | null;

  @ApiModelProperty()
  @Column('varchar', {
    nullable: true,
    length: 3,
    name: 'CED_A_SERI',
  })
  CED_A_SERI: string | null;

  @ApiModelProperty()
  @Column('varchar', {
    nullable: true,
    length: 1,
    name: 'CED_A_SEXO',
  })
  CED_A_SEXO: string | null;

  @ApiModelPropertyOptional()
  @Column('varchar', {
    nullable: true,
    length: 25,
    name: 'CALLE',
  })
  CALLE: string | null;

  @ApiModelPropertyOptional()
  @Column('varchar', {
    nullable: true,
    length: 5,
    name: 'CASA',
  })
  CASA: string | null;

  @ApiModelPropertyOptional()
  @Column('varchar', {
    nullable: true,
    length: 25,
    name: 'EDIFICIO',
  })
  EDIFICIO: string | null;

  @ApiModelPropertyOptional()
  @Column('varchar', {
    nullable: true,
    length: 2,
    name: 'PISO',
  })
  PISO: string | null;

  @ApiModelPropertyOptional()
  @Column('varchar', {
    nullable: true,
    length: 5,
    name: 'APTO',
  })
  APTO: string | null;

  @ApiModelPropertyOptional()
  @Column('varchar', {
    nullable: true,
    length: 20,
    name: 'TELEFONO',
  })
  TELEFONO: string | null;

  @ApiModelPropertyOptional()
  @Column('varchar', {
    nullable: true,
    length: 3,
    name: 'COD_MUNICIPIO',
  })
  COD_MUNICIPIO: string | null;

  @ApiModelPropertyOptional()
  @Column('varchar', {
    nullable: true,
    length: 2,
    name: 'COD_CIUDAD',
  })
  COD_CIUDAD: string | null;

  @ApiModelPropertyOptional()
  @Column('varchar', {
    nullable: true,
    length: 4,
    name: 'COD_SECTOR',
  })
  COD_SECTOR: string | null;

  @ApiModelPropertyOptional()
  @Column('numeric', {
    nullable: true,
    precision: 2,
    scale: 0,
    name: 'COD_PIEL',
  })
  COD_PIEL: number | null;

  @ApiModelPropertyOptional()
  @Column('numeric', {
    nullable: true,
    precision: 2,
    scale: 0,
    name: 'COD_SANGRE',
  })
  COD_SANGRE: number | null;

  @ApiModelPropertyOptional()
  @Column('varchar', {
    nullable: true,
    length: 1,
    name: 'SEXO',
  })
  SEXO: string | null;

  @ApiModelPropertyOptional()
  @Column('varchar', {
    nullable: true,
    length: 25,
    name: 'SENAS_PART',
  })
  SENAS_PART: string | null;

  @ApiModelPropertyOptional()
  @Column('numeric', {
    nullable: true,
    precision: 3,
    scale: 0,
    name: 'COD_OCUP',
  })
  COD_OCUP: number | null;

  @ApiModelPropertyOptional()
  @Column('numeric', {
    nullable: true,
    precision: 3,
    scale: 0,
    name: 'COD_NACION',
  })
  COD_NACION: number | null;

  @ApiModelPropertyOptional()
  @Column('varchar', {
    nullable: true,
    length: 1,
    name: 'EST_CIVIL',
  })
  EST_CIVIL: string | null;

  @ApiModelPropertyOptional()
  @Column('varchar', {
    nullable: true,
    length: 12,
    name: 'NUM_PASAPO',
  })
  NUM_PASAPO: string | null;

  @ApiModelPropertyOptional()
  @Column('varchar', {
    nullable: true,
    length: 12,
    name: 'NUM_RESIDE',
  })
  NUM_RESIDE: string | null;

  @ApiModelPropertyOptional()
  @Column('datetime', {
    nullable: true,
    name: 'FECHA_EXPIRACION',
  })
  FECHA_EXPIRACION: Date | null;

  @ApiModelPropertyOptional()
  @Column('varchar', {
    nullable: true,
    length: 3,
    name: 'COD_MUNICIPIO_ELECTORAL',
  })
  COD_MUNICIPIO_ELECTORAL: string | null;

  @ApiModelPropertyOptional()
  @Column('varchar', {
    nullable: true,
    length: 5,
    name: 'COD_RECINTO_ELECTORAL',
  })
  COD_RECINTO_ELECTORAL: string | null;

  @ApiModelPropertyOptional()
  @Column('varchar', {
    nullable: true,
    length: 5,
    name: 'COD_RECINTO',
  })
  COD_RECINTO: string | null;

  @ApiModelPropertyOptional()
  @Column('varchar', {
    nullable: true,
    length: 6,
    name: 'COLEGIO',
  })
  COLEGIO: string | null;

  @ApiModelPropertyOptional()
  @Column('varchar', {
    nullable: true,
    length: 1,
    name: 'CATEGORIA',
  })
  CATEGORIA: string | null;

  @ApiModelPropertyOptional()
  @Column('varchar', {
    nullable: true,
    length: 2,
    name: 'TIPO_CAUSA',
  })
  TIPO_CAUSA: string | null;

  @ApiModelPropertyOptional()
  @Column('numeric', {
    nullable: true,
    precision: 2,
    scale: 0,
    name: 'COD_CAUSA',
  })
  COD_CAUSA: number | null;

  @ApiModelPropertyOptional()
  @Column('varchar', {
    nullable: true,
    length: 14,
    name: 'SOLICITUD',
  })
  SOLICITUD: string | null;

  @ApiModelPropertyOptional()
  @Column('varchar', {
    nullable: true,
    length: 1,
    name: 'ESTATUS',
  })
  ESTATUS: string | null;

  @ApiModelPropertyOptional()
  @Column('varchar', {
    nullable: true,
    length: 12,
    name: 'SOLICITUD_DOW',
  })
  SOLICITUD_DOW: string | null;

  @ApiModelPropertyOptional()
  @Column('datetime', {
    nullable: true,
    name: 'FECHA_CANCELACION',
  })
  FECHA_CANCELACION: Date | null;

  @ApiModelPropertyOptional()
  @Column('numeric', {
    nullable: true,
    precision: 6,
    scale: 0,
    name: 'NUMERO_OFICIO',
  })
  NUMERO_OFICIO: number | null;

  @ApiModelPropertyOptional()
  @Column('datetime', {
    nullable: true,
    name: 'FECHA_REVALIDACION',
  })
  FECHA_REVALIDACION: Date | null;

  @ApiModelPropertyOptional()
  @Column('numeric', {
    nullable: true,
    precision: 6,
    scale: 0,
    name: 'NUMERO_OFICIO_REVALIDACION',
  })
  NUMERO_OFICIO_REVALIDACION: number | null;

  @ApiModelPropertyOptional()
  @Column('varchar', {
    nullable: true,
    length: 1,
    name: 'TIPO_ACTA',
  })
  TIPO_ACTA: string | null;

  @ApiModelPropertyOptional()
  @Column('varchar', {
    nullable: true,
    length: 3,
    name: 'ACTA_MUN',
  })
  ACTA_MUN: string | null;

  @ApiModelPropertyOptional()
  @Column('varchar', {
    nullable: true,
    length: 2,
    name: 'ACTA_OFIC',
  })
  ACTA_OFIC: string | null;

  @ApiModelPropertyOptional()
  @Column('varchar', {
    nullable: true,
    length: 4,
    name: 'ACTA_LIBRO',
  })
  ACTA_LIBRO: string | null;

  @ApiModelPropertyOptional()
  @Column('varchar', {
    nullable: true,
    length: 3,
    name: 'ACTA_FOLIO',
  })
  ACTA_FOLIO: string | null;

  @ApiModelPropertyOptional()
  @Column('varchar', {
    nullable: true,
    length: 5,
    name: 'ACTA_NUMERO',
  })
  ACTA_NUMERO: string | null;

  @ApiModelPropertyOptional()
  @Column('varchar', {
    nullable: true,
    length: 4,
    name: 'ACTA_ANO',
  })
  ACTA_ANO: string | null;

  @ApiModelPropertyOptional()
  @Column('varchar', {
    nullable: true,
    length: 1,
    name: 'TIPO_LIBRO',
  })
  TIPO_LIBRO: string | null;

  @ApiModelPropertyOptional()
  @Column('varchar', {
    nullable: true,
    length: 50,
    name: 'ACTA_AUTOMATIZADA',
  })
  ACTA_AUTOMATIZADA: string | null;

  @ApiModelPropertyOptional()
  @Column('char', {
    nullable: true,
    name: 'DONANTE',
  })
  DONANTE: string | null;
}
