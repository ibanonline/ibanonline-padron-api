import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';

@Entity('CIUDAD_EXTERIOR', { schema: 'dbo' })
export class CiudadExterior {
  @Column('varchar', {
    nullable: false,
    primary: true,
    length: 3,
    name: 'cod_municipio',
  })
  cod_municipio: string;

  @Column('varchar', {
    nullable: false,
    primary: true,
    length: 2,
    name: 'cod_ciudad',
  })
  cod_ciudad: string;

  @Column('varchar', {
    nullable: true,
    length: 50,
    name: 'descripcion',
  })
  descripcion: string | null;
}
