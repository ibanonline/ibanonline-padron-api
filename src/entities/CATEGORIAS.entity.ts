import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';

@Entity('CATEGORIAS', { schema: 'dbo' })
export class Categorias {
  @Column('varchar', {
    nullable: false,
    length: 1,
    name: 'CATEGORIA',
  })
  CATEGORIA: string;

  @Column('varchar', {
    nullable: true,
    length: 20,
    name: 'DESCRIPCION',
  })
  DESCRIPCION: string | null;
}
