import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';

@Entity('RND_GENERATED_DATA', { schema: 'dbo' })
export class RndGeneratedData {
  @Column('varchar', {
    nullable: false,
    primary: true,
    length: 3,
    name: 'MUN_CED',
  })
  MUN_CED: string;

  @Column('varchar', {
    nullable: false,
    primary: true,
    length: 7,
    name: 'SEQ_CED',
  })
  SEQ_CED: string;

  @Column('varchar', {
    nullable: false,
    primary: true,
    length: 1,
    name: 'VER_CED',
  })
  VER_CED: string;

  @Column('varchar', {
    nullable: true,
    length: 100,
    name: 'NOMBRES',
  })
  NOMBRES: string | null;

  @Column('varchar', {
    nullable: true,
    length: 100,
    name: 'APELLIDO1',
  })
  APELLIDO1: string | null;

  @Column('datetime', {
    nullable: false,
    name: 'DATA_DATE',
  })
  DATA_DATE: Date;

  @Column('char', {
    nullable: true,
    name: 'REPORTED',
  })
  REPORTED: string | null;
}
