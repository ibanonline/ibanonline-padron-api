import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';

@Entity('RECINTOS_EXTERIOR', { schema: 'dbo' })
export class RecintosExterior {
  @Column('varchar', {
    nullable: false,
    primary: true,
    length: 5,
    name: 'cod_recinto',
  })
  cod_recinto: string;

  @Column('varchar', {
    nullable: false,
    primary: true,
    length: 3,
    name: 'cod_municipio',
  })
  cod_municipio: string;

  @Column('varchar', {
    nullable: true,
    length: 2,
    name: 'cod_ciudad',
  })
  cod_ciudad: string | null;

  @Column('varchar', {
    nullable: true,
    length: 100,
    name: 'descripcion',
  })
  descripcion: string | null;

  @Column('nvarchar', {
    nullable: true,
    length: 100,
    name: 'direccion',
  })
  direccion: string | null;

  @Column('char', {
    nullable: true,
    name: 'estatus',
  })
  estatus: string | null;
}
