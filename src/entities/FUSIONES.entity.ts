import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';

@Entity('FUSIONES', { schema: 'dbo' })
export class Fusiones {
  @Column('varchar', {
    nullable: false,
    primary: true,
    length: 3,
    name: 'COD_MUNICIPIO',
  })
  COD_MUNICIPIO: string;

  @Column('varchar', {
    nullable: false,
    primary: true,
    length: 5,
    name: 'COD_RECINTO',
  })
  COD_RECINTO: string;

  @Column('varchar', {
    nullable: false,
    primary: true,
    length: 6,
    name: 'COLE_ORIGEN',
  })
  COLE_ORIGEN: string;

  @Column('varchar', {
    nullable: true,
    length: 5,
    name: 'RECINTO_DESTINO',
  })
  RECINTO_DESTINO: string | null;

  @Column('varchar', {
    nullable: true,
    length: 6,
    name: 'COLE_DESTINO',
  })
  COLE_DESTINO: string | null;

  @Column('int', {
    nullable: true,
    name: 'INSC_ORIGEN',
  })
  INSC_ORIGEN: number | null;
}
