import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';

@Entity('FUSIONES_INHABILITADOS', { schema: 'dbo' })
export class FusionesInhabilitados {
  @Column('varchar', {
    nullable: true,
    length: 2,
    name: 'cod_provincia',
  })
  cod_provincia: string | null;

  @Column('varchar', {
    nullable: true,
    length: 3,
    name: 'cod_municipio',
  })
  cod_municipio: string | null;

  @Column('varchar', {
    nullable: true,
    length: 2,
    name: 'cod_circunscripcion',
  })
  cod_circunscripcion: string | null;

  @Column('varchar', {
    nullable: true,
    length: 5,
    name: 'cod_recinto',
  })
  cod_recinto: string | null;

  @Column('varchar', {
    nullable: true,
    length: 6,
    name: 'colegio',
  })
  colegio: string | null;

  @Column('varchar', {
    nullable: false,
    primary: true,
    length: 3,
    name: 'mun_ced',
  })
  mun_ced: string;

  @Column('varchar', {
    nullable: false,
    primary: true,
    length: 7,
    name: 'seq_ced',
  })
  seq_ced: string;

  @Column('varchar', {
    nullable: false,
    primary: true,
    length: 1,
    name: 'ver_ced',
  })
  ver_ced: string;

  @Column('varchar', {
    nullable: true,
    length: 30,
    name: 'nombres',
  })
  nombres: string | null;

  @Column('varchar', {
    nullable: true,
    length: 30,
    name: 'apellido1',
  })
  apellido1: string | null;

  @Column('varchar', {
    nullable: true,
    length: 30,
    name: 'apellido2',
  })
  apellido2: string | null;

  @Column('datetime', {
    nullable: true,
    name: 'fecha_nac',
  })
  fecha_nac: Date | null;

  @Column('numeric', {
    nullable: true,
    precision: 3,
    scale: 0,
    name: 'cod_nacion',
  })
  cod_nacion: number | null;

  @Column('varchar', {
    nullable: true,
    length: 1,
    name: 'sexo',
  })
  sexo: string | null;

  @Column('varchar', {
    nullable: true,
    length: 1,
    name: 'categoria',
  })
  categoria: string | null;

  @Column('varchar', {
    nullable: true,
    length: 2,
    name: 'tipo_causa',
  })
  tipo_causa: string | null;

  @Column('numeric', {
    nullable: true,
    precision: 2,
    scale: 0,
    name: 'cod_causa',
  })
  cod_causa: number | null;

  @Column('varchar', {
    nullable: true,
    length: 3,
    name: 'cod_municipio_origen',
  })
  cod_municipio_origen: string | null;

  @Column('varchar', {
    nullable: true,
    length: 6,
    name: 'colegio_origen',
  })
  colegio_origen: string | null;

  @Column('int', {
    nullable: false,
    name: 'pos_pagina',
  })
  pos_pagina: number;
}
