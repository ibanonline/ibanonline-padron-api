import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';

@Entity('NOMINA_CIRC_01', { schema: 'dbo' })
export class NominaCir01 {
  @Column('varchar', {
    nullable: true,
    length: 100,
    name: 'CEDULA',
  })
  CEDULA: string | null;

  @Column('varchar', {
    nullable: true,
    length: 100,
    name: 'NOMBRES',
  })
  NOMBRES: string | null;
}
