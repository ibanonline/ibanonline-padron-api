import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';

@Entity('RECINTOS', { schema: 'dbo' })
export class Recintos {
  @Column('varchar', {
    nullable: false,
    primary: true,
    length: 5,
    name: 'COD_RECINTO',
  })
  COD_RECINTO: string;

  @Column('varchar', {
    nullable: false,
    primary: true,
    length: 3,
    name: 'COD_MUNICIPIO',
  })
  COD_MUNICIPIO: string;

  @Column('varchar', {
    nullable: true,
    length: 60,
    name: 'DESCRIPCION',
  })
  DESCRIPCION: string | null;

  @Column('varchar', {
    nullable: true,
    length: 60,
    name: 'DIRECCION',
  })
  DIRECCION: string | null;

  @Column('varchar', {
    nullable: true,
    length: 2,
    name: 'COD_CIUDAD',
  })
  COD_CIUDAD: string | null;

  @Column('varchar', {
    nullable: true,
    length: 4,
    name: 'COD_SECTOR',
  })
  COD_SECTOR: string | null;

  @Column('varchar', {
    nullable: true,
    length: 2,
    name: 'COD_CIRCUNSCRIPCION',
  })
  COD_CIRCUNSCRIPCION: string | null;

  @Column('int', {
    nullable: true,
    name: 'COD_BARRIO',
  })
  COD_BARRIO: number | null;

  @Column('int', {
    nullable: true,
    name: 'CAPACIDAD_RECINTO',
  })
  CAPACIDAD_RECINTO: number | null;

  @Column('numeric', {
    nullable: true,
    precision: 18,
    scale: 0,
    name: 'OFICIO',
  })
  OFICIO: number | null;

  @Column('varchar', {
    nullable: true,
    length: 1,
    name: 'ESTATUS',
  })
  ESTATUS: string | null;
}
