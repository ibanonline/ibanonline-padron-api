import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';

@Entity('MUNICIPIO', { schema: 'dbo' })
export class Municipio {
  @Column('varchar', {
    nullable: false,
    primary: true,
    length: 3,
    name: 'COD_MUNICIPIO',
  })
  COD_MUNICIPIO: string;

  @Column('varchar', {
    nullable: true,
    length: 35,
    name: 'DESCRIPCION',
  })
  DESCRIPCION: string | null;

  @Column('varchar', {
    nullable: false,
    length: 2,
    name: 'COD_PROVINCIA',
  })
  COD_PROVINCIA: string;

  @Column('varchar', {
    nullable: true,
    length: 3,
    name: 'COD_MUNICIPIO_PADRE',
  })
  COD_MUNICIPIO_PADRE: string | null;

  @Column('numeric', {
    nullable: true,
    precision: 18,
    scale: 0,
    name: 'OFICIO',
  })
  OFICIO: number | null;

  @Column('varchar', {
    nullable: true,
    length: 1,
    name: 'ESTATUS',
  })
  ESTATUS: string | null;

  @Column('varchar', {
    nullable: true,
    length: 1,
    name: 'DM',
  })
  DM: string | null;
}
