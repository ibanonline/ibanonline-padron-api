import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';

@Entity('NACIONALIDAD', { schema: 'dbo' })
export class Nacionalidad {
  @Column('numeric', {
    nullable: false,
    primary: true,
    precision: 3,
    scale: 0,
    name: 'COD_NACION',
  })
  COD_NACION: number;

  @Column('varchar', {
    nullable: true,
    length: 40,
    name: 'DESCRIPCION',
  })
  DESCRIPCION: string | null;

  @Column('varchar', {
    nullable: true,
    length: 50,
    name: 'NOMBREPAIS',
  })
  NOMBREPAIS: string | null;

  @Column('char', {
    nullable: true,
    length: 3,
    name: 'ACCESOINTERNACIONAL',
  })
  ACCESOINTERNACIONAL: string | null;
}
