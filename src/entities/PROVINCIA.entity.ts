import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';

@Entity('PROVINCIA', { schema: 'dbo' })
export class Provincia {
  @Column('varchar', {
    nullable: false,
    primary: true,
    length: 2,
    name: 'COD_PROVINCIA',
  })
  COD_PROVINCIA: string;

  @Column('varchar', {
    nullable: true,
    length: 30,
    name: 'DESCRIPCION',
  })
  DESCRIPCION: string | null;

  @Column('numeric', {
    nullable: true,
    precision: 18,
    scale: 0,
    name: 'OFICIO',
  })
  OFICIO: number | null;

  @Column('varbinary', {
    nullable: true,
    name: 'ESTATUS',
  })
  ESTATUS: Buffer | null;

  @Column('varchar', {
    nullable: true,
    length: 2,
    name: 'ZONA',
  })
  ZONA: string | null;
}
