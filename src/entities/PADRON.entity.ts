import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';

@Entity('PADRON', { schema: 'dbo' })
@Index('IX_PADRON', [
  'Lugar_votacion',
  'cod_municipio',
  'colegio',
  'pos_pagina',
])
@Index('IX_PADRON_1', [
  'Lugar_votacion',
  'Cod_municipio_exterior',
  'Colegio_exterior',
  'Pos_pagina_exterior',
])
export class Padron {
  @Column('varchar', {
    nullable: true,
    length: 2,
    name: 'cod_provincia',
  })
  cod_provincia: string | null;

  @Column('varchar', {
    nullable: true,
    length: 3,
    name: 'cod_municipio',
  })
  cod_municipio: string | null;

  @Column('varchar', {
    nullable: true,
    length: 2,
    name: 'cod_circunscripcion',
  })
  cod_circunscripcion: string | null;

  @Column('varchar', {
    nullable: true,
    length: 5,
    name: 'cod_recinto',
  })
  cod_recinto: string | null;

  @Column('varchar', {
    nullable: true,
    length: 6,
    name: 'colegio',
  })
  colegio: string | null;

  @Column('varchar', {
    nullable: false,
    primary: true,
    length: 3,
    name: 'mun_ced',
  })
  mun_ced: string;

  @Column('varchar', {
    nullable: false,
    primary: true,
    length: 7,
    name: 'seq_ced',
  })
  seq_ced: string;

  @Column('varchar', {
    nullable: false,
    primary: true,
    length: 1,
    name: 'ver_ced',
  })
  ver_ced: string;

  @Column('varchar', {
    nullable: true,
    length: 30,
    name: 'nombres',
  })
  nombres: string | null;

  @Column('varchar', {
    nullable: true,
    length: 30,
    name: 'apellido1',
  })
  apellido1: string | null;

  @Column('varchar', {
    nullable: true,
    length: 30,
    name: 'apellido2',
  })
  apellido2: string | null;

  @Column('datetime', {
    nullable: true,
    name: 'fecha_nac',
  })
  fecha_nac: Date | null;

  @Column('decimal', {
    nullable: true,
    precision: 3,
    scale: 0,
    name: 'cod_nacion',
  })
  cod_nacion: number | null;

  @Column('varchar', {
    nullable: true,
    length: 1,
    name: 'sexo',
  })
  sexo: string | null;

  @Column('varchar', {
    nullable: true,
    length: 1,
    name: 'categoria',
  })
  categoria: string | null;

  @Column('varchar', {
    nullable: true,
    length: 2,
    name: 'tipo_causa',
  })
  tipo_causa: string | null;

  @Column('decimal', {
    nullable: true,
    precision: 2,
    scale: 0,
    name: 'cod_causa',
  })
  cod_causa: number | null;

  @Column('varchar', {
    nullable: true,
    length: 3,
    name: 'cod_municipio_origen',
  })
  cod_municipio_origen: string | null;

  @Column('varchar', {
    nullable: true,
    length: 6,
    name: 'colegio_origen',
  })
  colegio_origen: string | null;

  @Column('int', {
    nullable: false,
    name: 'pos_pagina',
  })
  pos_pagina: number;

  @Column('char', {
    nullable: true,
    name: 'Lugar_votacion',
  })
  Lugar_votacion: string | null;

  @Column('varchar', {
    nullable: true,
    length: 2,
    name: 'Provincia_exterior',
  })
  Provincia_exterior: string | null;

  @Column('varchar', {
    nullable: true,
    length: 3,
    name: 'Cod_municipio_exterior',
  })
  Cod_municipio_exterior: string | null;

  @Column('varchar', {
    nullable: true,
    length: 5,
    name: 'Recinto_exterior',
  })
  Recinto_exterior: string | null;

  @Column('varchar', {
    nullable: true,
    length: 6,
    name: 'Colegio_exterior',
  })
  Colegio_exterior: string | null;

  @Column('int', {
    nullable: true,
    name: 'Pos_pagina_exterior',
  })
  Pos_pagina_exterior: number | null;
}
