import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';

@Entity('SANGRE', { schema: 'dbo' })
export class Sangre {
  @Column('numeric', {
    nullable: false,
    precision: 2,
    scale: 0,
    name: 'COD_SANGRE',
  })
  COD_SANGRE: number;

  @Column('varchar', {
    nullable: true,
    length: 5,
    name: 'DESCRIPCION',
  })
  DESCRIPCION: string | null;
}
