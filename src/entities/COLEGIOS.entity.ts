import {
  BaseEntity,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';

@Entity('COLEGIOS', { schema: 'dbo' })
export class Colegios {
  @Column('varchar', {
    nullable: false,
    primary: true,
    length: 6,
    name: 'COLEGIO',
  })
  COLEGIO: string;

  @Column('varchar', {
    nullable: false,
    primary: true,
    length: 3,
    name: 'COD_MUNICIPIO',
  })
  COD_MUNICIPIO: string;

  @Column('varchar', {
    nullable: true,
    length: 60,
    name: 'DESCRIPCION',
  })
  DESCRIPCION: string | null;

  @Column('varchar', {
    nullable: false,
    length: 5,
    name: 'COD_RECINTO',
  })
  COD_RECINTO: string;

  @Column('varchar', {
    nullable: true,
    length: 1,
    default: () => "'S'",
    name: 'TIENECUPO',
  })
  TIENECUPO: string | null;

  @Column('numeric', {
    nullable: true,
    default: () => '(0)',
    precision: 5,
    scale: 0,
    name: 'CANT_INSCRITOS',
  })
  CANT_INSCRITOS: number | null;

  @Column('numeric', {
    nullable: true,
    default: () => '(0)',
    precision: 5,
    scale: 0,
    name: 'CANT_RESERVADA',
  })
  CANT_RESERVADA: number | null;

  @Column('int', {
    nullable: true,
    name: 'ELECTORESLocales',
  })
  ELECTORESLocales: number | null;

  @Column('int', {
    nullable: true,
    name: 'ELECTORESExterior',
  })
  ELECTORESExterior: number | null;

  @Column('int', {
    nullable: true,
    name: 'ELECTORESInhabilitados',
  })
  ELECTORESInhabilitados: number | null;
}
