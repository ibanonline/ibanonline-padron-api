FROM node:10-alpine

COPY package.json .
COPY package-lock.json .

RUN apk update && apk upgrade \
	&& apk add --no-cache git \
	&& apk --no-cache add --virtual builds-deps build-base python \
  && npm config set unsafe-perm true \
	&& npm i npm@latest -g

RUN npm i --production

COPY . .

EXPOSE 3000

CMD ["npm", "start"]
